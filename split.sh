#!/bin/bash
# tiles the pdf document with gameboard and cards to A4 pages with 10 % print margins.

# script suppose the project contains 3x11 pages with size of A4 minus 10% margins, that is:
# A4 size: 210 x 297mm
# A4 minus 10 % margins (*0.9): 189 x 267.3 mm
# A4 minus 10 % margins (*0.9) with 4x5 panels (*4, *5): 756 x 1069.2 mm

# english version
# tile the poster to smaller pages
mutool poster -x 3 -y 11 monocraft_en.pdf tmp1.pdf
# resize the page back to A4 and scale the content down to proper size (0.9):
pdfjam --outfile tmp2.pdf --scale 0.9 --papersize '{210mm,297mm}' tmp1.pdf
# split pages according single/double/color_papers printing
pdfjam tmp2.pdf 1-9   -o public/print_files_en/monocraft_en_print_single_side_1_copy.pdf 
pdfjam tmp2.pdf 10-17 -o public/print_files_en/monocraft_en_print_double_side_1_copy.pdf 
pdfjam tmp2.pdf 18    -o public/print_files_en/monocraft_en_lifes_optA_brown_paper_3_copies.pdf 
pdfjam tmp2.pdf 19    -o public/print_files_en/monocraft_en_lifes_optA_black_paper_3_copies.pdf 
pdfjam tmp2.pdf 20    -o public/print_files_en/monocraft_en_lifes_optA_light_grey_paper_3_copies.pdf 
pdfjam tmp2.pdf 21    -o public/print_files_en/monocraft_en_lifes_optA_yellow_paper_3_copies.pdf 
pdfjam tmp2.pdf 22    -o public/print_files_en/monocraft_en_lifes_optA_light_blue_paper_3_copies.pdf 
pdfjam tmp2.pdf 23    -o public/print_files_en/monocraft_en_lifes_optA_green_paper_3_copies.pdf 
pdfjam tmp2.pdf 24    -o public/print_files_en/monocraft_en_lifes_optA_dark_gray_paper_3_copies.pdf 
pdfjam tmp2.pdf 25-31 -o public/print_files_en/monocraft_en_lifes_optB_single_side_3_copies.pdf 
rm tmp1.pdf
rm tmp2.pdf

# czech version
# tile the poster to smaller pages
mutool poster -x 3 -y 11 monocraft_cz.pdf tmp1.pdf
# resize the page back to A4 and scale the content down to proper size (0.9):
pdfjam --outfile tmp2.pdf --scale 0.9 --papersize '{210mm,297mm}' tmp1.pdf
# split pages according single/double/color_papers printing
pdfjam tmp2.pdf 1-9   -o public/print_files_cz/monocraft_cz_print_jednostranne_1_kopie.pdf 
pdfjam tmp2.pdf 10-17 -o public/print_files_cz/monocraft_cz_print_oboustranne_1_kopie.pdf 
pdfjam tmp2.pdf 18    -o public/print_files_cz/monocraft_cz_zivoty_varA_hnedy_papir_3_kopie.pdf 
pdfjam tmp2.pdf 19    -o public/print_files_cz/monocraft_cz_zivoty_varA_cerny_papir_3_kopie.pdf 
pdfjam tmp2.pdf 20    -o public/print_files_cz/monocraft_cz_zivoty_varA_svetle_sedy_papir_3_kopie.pdf 
pdfjam tmp2.pdf 21    -o public/print_files_cz/monocraft_cz_zivoty_varA_zluty_papir_3_kopie.pdf 
pdfjam tmp2.pdf 22    -o public/print_files_cz/monocraft_cz_zivoty_varA_svetle_modry_papir_3_kopie.pdf 
pdfjam tmp2.pdf 23    -o public/print_files_cz/monocraft_cz_zivoty_varA_zeleny_papir_3_kopie.pdf 
pdfjam tmp2.pdf 24    -o public/print_files_cz/monocraft_cz_zivoty_varA_tmave_sedy_papir_3_kopie.pdf 
pdfjam tmp2.pdf 25-31 -o public/print_files_cz/monocraft_cz_zivoty_varB_jednostranne_3_kopie.pdf 
rm tmp1.pdf
rm tmp2.pdf
