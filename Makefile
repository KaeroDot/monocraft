draft:
	# pandoc:
	#	-f: format is markdown github
	#	--css: css style sheet
	#	-s: standalone
	pandoc -f markdown_github+markdown_in_html_blocks --css pandoc.css -s -o public/index.html readme.md
	pandoc -f markdown_github+markdown_in_html_blocks --css pandoc.css -s -o public/index_cz.html readme_cz.md
	sed -i 's/readme_cz.md/index_cz.html/' public/index.html
	sed -i 's/readme.md/index.html/' public/index_cz.html
