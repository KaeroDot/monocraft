# MONOCRAFT
![MONOCRAFT](images/monocraft.jpg)

**[Česky](readme_cz.md)**

Monopoly like game in Minecraft style.

[![Game board](images/gameboard.jpg)](images/gameboard.jpg) [![Photo of the game](images/monocraft-beta_version.jpg)](images/monocraft-beta_version.jpg)

## How to make
1. **Print** file
   [monocraft_en_print_single_side_1_copy](print_files_en/monocraft_en_print_single_side_1_copy.pdf)
   to thick glossy A4 papers in single side mode, without any resizing or rescaling.
2. **Print** file
   [monocraft_en_print_double_side_1_copy](print_files_en/monocraft_en_print_double_side_1_copy.pdf)
   to thick glossy A4 papers in double side mode, without any resizing or rescaling.
3. **Print** lifenotes (banknotes). You got two options. Either you get coloured paper (option A), or
   you will spent a lot of colour printer tonner (option B).
    1. **Option A**: print following files on respectively coloured thin A4 papers, one side mode, without any
       resizing or rescaling. Each file print in 3 copies:
        1. [monocraft_en_lifes_optA_black_paper_3_copies](print_files_en/monocraft_en_lifes_optA_black_paper_3_copies.pdf)
        1. [monocraft_en_lifes_optA_brown_paper_3_copies](print_files_en/monocraft_en_lifes_optA_brown_paper_3_copies.pdf)
        1. [monocraft_en_lifes_optA_dark_gray_paper_3_copies](print_files_en/monocraft_en_lifes_optA_dark_gray_paper_3_copies.pdf)
        1. [monocraft_en_lifes_optA_green_paper_3_copies](print_files_en/monocraft_en_lifes_optA_green_paper_3_copies.pdf)
        1. [monocraft_en_lifes_optA_light_blue_paper_3_copies](print_files_en/monocraft_en_lifes_optA_light_blue_paper_3_copies.pdf)
        1. [monocraft_en_lifes_optA_light_grey_paper_3_copies](print_files_en/monocraft_en_lifes_optA_light_grey_paper_3_copies.pdf)
        1. [monocraft_en_lifes_optA_yellow_paper_3_copies](print_files_en/monocraft_en_lifes_optA_yellow_paper_3_copies.pdf)
    2. **Option B**: print following file on white thin A4 papers, one side mode, without any resizing or
       rescaling, in 3 copies:
       [monocraft_en_lifes_optB_single_side](print_files_en/monocraft_en_lifes_optB_single_side.pdf)
1. **Cut out** board and glue parts together.
1. **Cut out** cards and lifenotes.
1. **Prepare** tokens, use anything you like (e.g. LEGO Minecraft figures). Prepare 32 small eggs (use
   anything you like, e.g. LEGO bricks 2x2) and 12 large eggs (e.g. LEGO bricks 2x4).
1. **Learn** rules.
1. **Play!**

## Rules
You play for your life represented by amount of hearts in your possession. If you step on field with
a creature, table or biome, you could loose life. You can get deeds to creatures and
plant eggs to make more powerful creatures.

For rules see Monopoly rules (e.g. [here](https://www.ultraboardgames.com/monopoly/game-rules.php)) with following changes:

1. Replace words _Houses_ by _Small Eggs_, denoted by green-black egg image ![Small egg](images/small_egg.jpg).
1. Replace words _Hotels_ by _Big Eggs_, denoted by yellow-blue egg image ![Large egg](images/large_egg.jpg).
1. Replace words _Title Deed card_ by _Creature card_.
1. Replace words _Money_ by _Life_, represented by red hearts image ![Life](images/heart.jpg).
1. Replace words _Jail_ by _End_, denoted by picture of End ![End](images/end.jpg).
1. Replace words _Free parking_ by _Free rest_.
1. Chance cards are marked by image of fishing rod ![Fishing rod](images/fishing_rod.jpg).
1. Community cards are marked by image of Ender chest ![Ender chest](images/ender_chest.jpg).

## Modifications
If you want to change something, do it!
All source files are part of git repository at GitLab: **[https://gitlab.com/KaeroDot/monocraft](https://gitlab.com/KaeroDot/monocraft)**.
You need:

1. [Inkscape](https://inkscape.org) or some other SVG editor.
1. Font _F77 Minecraft_ from [Fonts2U](https://fonts2u.com/f77-minecraft-regular.font). Save file `F77-Minecraft.ttf` to directory `~/.fonts`.
1. [mutool poster](https://www.mupdf.com/docs/manual-mutool-poster.html) tool (ubuntu installation:
   `sudo apt install mupdf-tools`).
1. [pdfjam](https://github.com/rrthomas/pdfjam) (ubuntu installation: `sudo apt install texlive-extra-utils`).

### How to:

1. Modify the source file `monocraft.svg`.
1. The file contains layers for czech and english languages. Hide unwanted layers.
1. Export the whole page to `monocraft_en.pdf` or `monocraft_cz.pdf` according the language.
1. Run script `split.sh` that cuts the pdf for A4 printing and properly rescales everything.

### Notes

- The `monocraft.svg` file contains grid with spacing 189 x 267.3 mm, that represents A4 pages without 10
  % reserved for printing margins. The layer _frames for positioning/debug_ contains rectangles aligned
  with the grid for proper positioning of the content. Show this layer for debugging, hide for final
  printout.
- If you add new pages, you have to modify the page size in `monocraft.svg` in multiples of 189 mm
  in x dimension and multiples of 267.3 mm in y dimension. You have to modify the script
  `split.sh`, where number of pages in x and y dimension is
  hardcoded.
- Users of operating system Windows: help yourself. I have no idea how to simply install required software in this
  OS, how to simply cut and resize pdfs into output files or how to automate the task by writing some script.

## License
MONOCRAFT is **NOT** officially tied to Monopoly, Hasbro, Minecraft nor Mojang Studios.

[The Monopoly title and game](https://monopoly.hasbro.com/) is licensed by [Hasbro](https://corporate.hasbro.com/en-us).
[The Minecraft title and game](https://www.minecraft.net/) is licensed by [Mojang Studios](https://www.minecraft.net/).
Thus MONOCRAFT can exist only if these two companies allows non-commercial modifications of
their assets, made by kids and for kids.
I hope so, because:

1. Mojang Studios is relaxed to modifications of Minecraft Name, Brand and Assets if 
   [these guidelines](https://account.mojang.com/documents/brand_guidelines) are met, and I believe
   I did so.
1. Monopoly game is about 100 years old, and all rules, boards and cards can be found on many, many
   webpages. So it seems fair to make a fan-made non-commercial derivative for home use.

The images used in the MONOCRAFT are taken from
[Minecraft Wiki](https://minecraft.fandom.com/wiki/Minecraft_Wiki), thus licensed by Mojang Studios
under [these terms and guidelines](https://account.mojang.com/documents/brand_guidelines).

The font used in the MONOCRAFT is *F77 Minecraft* by *Se7enty-Se7en* with Freeware
license available at [Fonts2U](https://fonts2u.com/f77-minecraft-regular.font).

Everything else created by me and my kids (general idea, lines, composition of elements, scripts etc.) is
licensed by [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license](https://creativecommons.org/licenses/by-sa/4.0/).

## Web page
[https://kaerodot.gitlab.io/monocraft/](https://kaerodot.gitlab.io/monocraft/)
