# MONOCRAFT
![MONOCRAFT](images/monocraft.jpg)

**[English](readme.md)**

Hra podobná Monopolům v Minecraft stylu.

[![Herní deska](images/hernideska.jpg)](images/hernideska.jpg) [![Fotografie hry](images/monocraft-beta_version.jpg)](images/monocraft-beta_version.jpg)

## Jak na to


1. **Vytiskněte** soubor 
   [monocraft_cz_print_jednostranne_1_kopie](print_files_cz/monocraft_cz_print_jednostranne_1_kopie.pdf)
   na tvrdé lesklé papíry A4 jednostranně, bez jakékoliv změny velikosti.
1. **Vytiskněte** soubor 
   [monocraft_cz_print_oboustranne_1_kopie](print_files_cz/monocraft_cz_print_oboustranne_1_kopie.pdf)
   na tvrdé lesklé papíry A4 oboustranně, bez jakékoliv změny velikosti.
1. **Vytiskněte** životy (bankovky). Jsou dvě možnosti. Buď použijete barevný papír (varianta A), nebo
   spotřebujete hodně barevného tonneru (varianta B).
    1. **Varianta A**: vytiskněte následující soubory na barevné tenké papíry A4, jednostranně, bez
       jakékoliv změny velikosti. Každý soubor tisknout ve 3 kopiích:
        1. [monocraft_cz_zivoty_varA_cerny_papir_3_kopie](print_files_cz/monocraft_cz_zivoty_varA_cerny_papir_3_kopie.pdf)
        1. [monocraft_cz_zivoty_varA_hnedy_papir_3_kopie](print_files_cz/monocraft_cz_zivoty_varA_hnedy_papir_3_kopie.pdf)
        1. [monocraft_cz_zivoty_varA_svetle_modry_papir_3_kopie](print_files_cz/monocraft_cz_zivoty_varA_svetle_modry_papir_3_kopie.pdf)
        1. [monocraft_cz_zivoty_varA_svetle_sedy_papir_3_kopie](print_files_cz/monocraft_cz_zivoty_varA_svetle_sedy_papir_3_kopie.pdf)
        1. [monocraft_cz_zivoty_varA_tmave_sedy_papir_3_kopie](print_files_cz/monocraft_cz_zivoty_varA_tmave_sedy_papir_3_kopie.pdf)
        1. [monocraft_cz_zivoty_varA_zeleny_papir_3_kopie](print_files_cz/monocraft_cz_zivoty_varA_zeleny_papir_3_kopie.pdf)
        1. [monocraft_cz_zivoty_varA_zluty_papir_3_kopie](print_files_cz/monocraft_cz_zivoty_varA_zluty_papir_3_kopie.pdf)
    1. **Varianta B**: vytiskněte následující soubor na bílé tenké papíry A4, jednostranně, bez
       jakékoliv změny velikosti, ve 3 kopiích:
       [monocraft_cz_zivoty_varB_jednostranne](print_files_cz/monocraft_cz_zivoty_varB_jednostranne.pdf)
1. **Vystřihněte** herní desku a slepte části desky k sobě.
1. **Vystřihněte** kartičky a životy.
1. **Sežeňte** nějaké hrací figurky (třeba LEGO figurky Minecraft). Připravte si 32 malých vajíček
   (třeba LEGO kostičky 2x2) a 12 velkých vajíček (třeba LEGO kostičky 2x4).
1. **Naučte** se pravidla.
1. **Hrejte!**

## Pravidla
Hrajete o život představovaných počtem vlastněných životů (srdíček). Když stoupnete na pole se
stvořením, stolem nebo biomem, přijít o životy. Stvoření si můžete kupovat. Také si můžete
kupovat vajíčka aby stvoření byly silnějši.

Hraje se podle pravidel hry Monopoly (viz třeba [tady](https://im9.cz/product-docs/tw4z3r4221v4ravu/Pravidla%20-%20Hasbro%20Monopoly.pdf)) s těmito změnami:

1. Slova `domy` zaměňte za slova malá vajíčka, 
1. Slova _domy_ zaměňte slovy _malá vajíčka_, označeno obrázkem černozeleného vajíčka ![Small egg](images/small_egg.jpg).
1. Slova _hotely_ zaměňte slovy _velká vajíčka_, označeno obrázkem žlutomodrého vajíčka ![Large egg](images/large_egg.jpg).
1. Slova _karty pozemků_ zaměňte slovy _karty stvoření_.
1. Slova _peníze_ zaměňte slovy _životy_, označeno obrázkem červeného srdíčka ![Life](images/heart.jpg).
1. Slova _vězení_ zaměňte slovy _End_, označeno obrázkem Endu ![End](images/end.jpg).
1. Slova _parkování zdarma_ zaměňte slovy _odpočinek zdarma_.
1. Karty náhod jsou označeny znakem rybářského prutu ![Fishing rod](images/fishing_rod.jpg).
1. Karty Pokladny jsou označeny znakem Ender truhly ![Ender chest](images/ender_chest.jpg).

Návod upravený pro Monocraft je taky [tady](rules/pravidla.pdf).

## Úpravy
Jestli chcete něco změnit, udělejte to!
Všechny zdrojové soubory jsou součástí git repozitáře na GitLabu: **[https://gitlab.com/KaeroDot/monocraft](https://gitlab.com/KaeroDot/monocraft)**.
Co budete potřebovat:

1. [Inkscape](https://inkscape.org) nebo jiný editor SVG souborů.
1. Písmo _F77 Minecraft_ ze stránky [Fonts2U](https://fonts2u.com/f77-minecraft-regular.font).
   Soubor `F77-Minecraft.ttf` uložte do adresáře `~/.fonts`.
1. Nástroj [mutool poster](https://www.mupdf.com/docs/manual-mutool-poster.html) (instalace v
   ubuntu: `sudo apt install mupdf-tools`).
1. Nástroj [pdfjam](https://github.com/rrthomas/pdfjam) (instalace v ubuntu: `sudo apt install
   texlive-extra-utils`).

### Jak na to:

1. Upravte soubor `monocraft.svg`.
1. Soubor obsahuje vrstvy pro český a anglický jazyk. Skryjte nepotřebnou vrstvu.
1. Exportujte celou stránku do souboru `monocraft_en.pdf` nebo `monocraft_cz.pdf`, podle zvoleného
   jazyka.
1. Spusťte script `split.sh`, který rozdělí stránku do několika A4 a vše přeškáluje jak je potřeba.

### Poznámky

- Soubor `monocraft.svg` obsahuje mřížku s rastrem 189 x 267.3 mm, která představuje stránky A4 bez
  10% okraje potřebného pro tisk. Vrstva _frames for positioning/debug_ obsahuje obdélníky zarovnané
  na mřížku, potřebné pro zarovnávání obsahu na stránky. Vrstvu nechte zobrazenou pro testování,
  skryjte ji pro tvorbu konečné verze. 
- Pokud přidáte nové stránky, musíte taky upravit velikost strany v souboru `monocraft.svg` v
  násobcích 189 mm pro rozměr v ose x a násobcích 267.3 mm pro rozměr v ose y. Taky je potřeba
  upravit script `split.sh`, kde je napsaný počet stran v rozměru
  x a y.
- Uživatelé operačního systému Windows: je to na Vás. Nemám ani představu, jak v tomto OS jednoduše
  nainstalovat potřebné programy, jak rozřezat pdf do výsledných souborů a jak to vše automatizovat
  napsáním skriptu.


## Licence
MONOCRAFT **NENÍ** oficiální součástí značek Monopoly, Hasbro, Minecraft nebo Mojang Studios.

[Název a hra Monopoly](https://monopoly.hasbro.com/) je licencován fy [Hasbro](https://corporate.hasbro.com/en-us).
[Název a hra Minecraft](https://www.minecraft.net/) je licencován fy [Mojang Studios](https://www.minecraft.net/).
Tedy existence MONOCRAFTu je možná pouze pokud tyto firmy umožňují nekomerční úpravy jejich intelektuálního vlastnictví, úpravy dětmi pro děti.
Doufám že je to možné, protože:

1. Mojang Studios explicitně umožňuje používání jejich materiálů za 
   [těchto podmínek](https://account.mojang.com/documents/brand_guidelines), které jsem myslím
   splnil.
1. Hra Monopoly je stará přibližně 100 let, a všechny návody, herní desky a karty je možné najít na
   mnoha, mnoha internetových stránkách. Tedy mi přijde v pořádku vytvářet fanouškovské nekomerční
   úpravy pro domácí použití.

Obrázky použité v MONOCRAFTu jsou převzaty z
[Minecraft Wiki](https://minecraft.fandom.com/wiki/Minecraft_Wiki), a tedy licencovány Mojang Studios
za [těchto podmínek](https://account.mojang.com/documents/brand_guidelines).

Použité písmo je *F77 Minecraft* vytvořené *Se7enty-Se7en* s licencí Freeware, dostupné na
[Fonts2U](https://fonts2u.com/f77-minecraft-regular.font).

Všechno ostaní vytvořené mnou a mými dětmi (celkové pojetí, čáry, sestavení, skritpy atd.) je licencováno pod
[Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license](https://creativecommons.org/licenses/by-sa/4.0/).

## Webová stránka
[https://kaerodot.gitlab.io/monocraft/](https://kaerodot.gitlab.io/monocraft/)
